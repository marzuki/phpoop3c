<?php

include_once "./Config/Database.php";

class Pegawai {

    public $conn;
    public function __construct()
    {
        $dbase = new Database();
        $db = $dbase->koneksi();
        $this->conn = $db;
    }

    public function tampil()
    {
        $sql = $this->conn->prepare('SELECT * FROM pegawai');
        $sql->execute();
        return $sql;
    }

}

?>